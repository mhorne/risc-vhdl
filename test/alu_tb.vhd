library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.riscv_defs.all;

entity alu_tb is
end alu_tb;

architecture main of alu_tb is
    constant period : time := 20 ns;

    signal clk : std_logic := '1';
    signal rst : std_logic;

    signal opcode : alu_opcode_t;
    signal data1 : src_data_t;
    signal data2 : src_data_t;
    signal alu_output : src_data_t;
    signal reg_output : src_data_t;

    signal udata1 : unsigned(REG_WIDTH-1 downto 0);
    signal udata2 : unsigned(REG_WIDTH-1 downto 0);

begin
    -- ALU Entity Instantiation --
    alu: entity work.alu(behave)
    port map (
        opcode => opcode,
        op1 => std_logic_vector(udata1),
        op2 => std_logic_vector(udata2),
        result => alu_output
    );

    -- Register the output --
    reg: entity work.buffer_reg(behave)
    port map (
        clk => clk,
        rst => rst,
        en => '1',
        data_in => alu_output,
        data_out => reg_output
    );

    clock: process is
    begin
        clk <= '1';
        loop
            clk <= not clk;
            wait for period / 2;
        end loop;
    end process;

    test_opcodes : process is
    begin
        udata1 <= resize("01110101", REG_WIDTH); -- 0x75
        udata2 <= resize("00010110", REG_WIDTH); -- 0x16
        rst <= '1';
        wait until rising_edge(clk);
        wait for period / 4;
        rst <= '0';
        wait for period / 2;

        wait until rising_edge(clk);

        -- opcodes --
        opcode <= ALU_OP_AND;
        wait until rising_edge(clk);
        opcode <= ALU_OP_OR;
        wait until rising_edge(clk);
        opcode <= ALU_OP_XOR;
        wait until rising_edge(clk);
        opcode <= ALU_OP_ADD;
        wait until rising_edge(clk);
        opcode <= ALU_OP_SUB;
        wait until rising_edge(clk);

        opcode <= ALU_OP_SLT;
        wait until rising_edge(clk);
        opcode <= ALU_OP_GE;
        wait until rising_edge(clk);
        opcode <= ALU_OP_SLTU;
        wait until rising_edge(clk);
        opcode <= ALU_OP_GEU;
        wait until rising_edge(clk);
        udata1 <= not udata1;
        udata2 <= not udata2;
        opcode <= ALU_OP_SLT;
        wait until rising_edge(clk);
        opcode <= ALU_OP_GE;
        wait until rising_edge(clk);
        opcode <= ALU_OP_SLTU;
        wait until rising_edge(clk);
        opcode <= ALU_OP_GEU;
        wait until rising_edge(clk);

        udata1 <= not udata1;
        udata2 <= resize("00000101", REG_WIDTH);
        opcode <= ALU_OP_SRA;
        wait until rising_edge(clk);
        udata1 <= not udata1;
        wait until rising_edge(clk);
        opcode <= ALU_OP_SRL;
        wait until rising_edge(clk);
        opcode <= ALU_OP_SLL;
        wait until rising_edge(clk);

        opcode <= ALU_OP_EQ;
        wait until rising_edge(clk);
        opcode <= ALU_OP_NEQ;
        wait until rising_edge(clk);
        udata1 <= udata2;
        opcode <= ALU_OP_EQ;
        wait until rising_edge(clk);
        opcode <= ALU_OP_NEQ;
        wait until rising_edge(clk);

        wait;
    end process;
end main;
