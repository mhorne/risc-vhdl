library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity shift_reg_tb is
end shift_reg_tb;

architecture test of shift_reg_tb is
    constant period : time := 20 ns;

    signal clk : std_logic;
    signal rst : std_logic;
    signal wr_en : std_logic;

    signal data : std_logic_vector(15 downto 0);
begin
    sreg4: entity work.shift_reg(behave)
    generic map (
        DATA_WIDTH => 16,
        LEVELS => 4
    )
    port map (
        clk => clk,
        rst => rst,
        en => wr_en,
        data_in => data
    );

    clock: process is
    begin
        clk <= '1';
        loop
            wait for period / 2;
            clk <= not clk;
        end loop;
    end process;

    test: process is
    begin
        wait until rising_edge(clk);
        rst <= '1';
        wait for period / 4;
        rst <= '0';
        wr_en <= '1';
        wait for period / 2;

        data <= "0000000011010110";
        wait for period;
        data <= "0000000010010111";
        wait for period;
        data <= "0000000000000000";
        wait;
    end process;

end architecture;
