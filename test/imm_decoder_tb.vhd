library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.riscv_defs.all;

entity imm_decoder_tb is
    constant period : time := 20 ns;
end imm_decoder_tb;

architecture main of imm_decoder_tb is
    signal clk : std_logic;

    signal imm_type : imm_type_t;
    signal imm_in : std_logic_vector(31 downto 7);
    signal imm_out : src_data_t;
begin

    decoder: entity work.imm_decoder(behave)
    port map (
        imm_type => imm_type,
        imm_in => imm_in,
        imm_out => imm_out
    );

    clock: process is
    begin
        clk <= '0';
        loop
            clk <= not clk;
            wait for period / 2;
        end loop;
    end process;

    test: process is
    begin
        wait for 0.5 * period;
        imm_in <= "1100110010110101000100101";

        imm_type <= IMM_NONE;
        wait for period;
        imm_type <= IMM_I;
        wait for period;
        imm_type <= IMM_S;
        wait for period;
        imm_type <= IMM_B;
        wait for period;
        imm_type <= IMM_U;
        wait for period;
        imm_type <= IMM_J;
        wait for period;

        wait;
    end process;
end main;
