library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.riscv_defs.all;

entity regfile_tb is
    constant period : time := 20 ns;
end regfile_tb;

architecture main of regfile_tb is
    signal r_addr1 : reg_addr_t;
    signal r_addr2 : reg_addr_t;
    signal input_data : src_data_t;
    signal w_addr : reg_addr_t;
    signal write : std_logic;
    signal clk : std_logic := '0';
    signal rst : std_logic := '1';
    signal out1 : src_data_t;
    signal out2 : src_data_t;

    signal data : unsigned(REG_WIDTH-1 downto 0);
begin
    registers: entity work.regfile(behave)
    port map (
        r_addr1 => r_addr1,
        r_addr2 => r_addr2,
        w_addr => w_addr,
        w_data => input_data,
        write => write,
        r_data1 => out1,
        r_data2 => out2,
        clk => clk,
        rst => rst
    );

    clock: process is
    begin
        loop
            clk <= not clk;
            wait for period / 2;
        end loop;
    end process;

    test: process is
    begin
        wait until rising_edge(clk);
        wait for 0.25 * period;
        rst <= '0';
        data <= resize("0110", data'length);
        wait for 0.5 * period;

        -- Write data to registers --
        write <= '1';
        for i in 0 to REGFILE_SIZE-1 loop
            w_addr <= std_logic_vector(to_unsigned(i, w_addr'length));
            data <= data + 1;
            input_data <= std_logic_vector(data);
            wait for period;
        end loop;

        -- Read data --
        write <= '0';
        for i in 1 to REGFILE_SIZE-1 loop
            r_addr1 <= std_logic_vector(to_unsigned(i, w_addr'length));
            r_addr2 <= std_logic_vector(to_unsigned(i-1, w_addr'length));
            wait for period;
        end loop;

        -- Read and write data --
        input_data <= std_logic_vector(to_unsigned(28484, input_data'length));
        w_addr <= std_logic_vector(to_unsigned(4, w_addr'length));
        write <= '1';
        r_addr1 <= std_logic_vector(to_unsigned(4, r_addr1'length));
        r_addr2 <= std_logic_vector(to_unsigned(6, r_addr2'length));
        wait for period;
    end process;
end main;
