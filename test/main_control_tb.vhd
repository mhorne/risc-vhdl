library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.riscv_defs.all;
use work.instr_defs.all;

entity main_control_tb is
    constant period : time := 20 ns;
end main_control_tb;

architecture main of main_control_tb is
    signal instr_input : instruction_t;
    signal clk : std_logic;
    signal rst : std_logic;
    signal src1 : alu_src1_t;
    signal src2 : alu_src2_t;
    signal mem_opcode : mem_op_t;
    signal pc_src : std_logic;
    signal reg_write : std_logic;
    signal immediate : src_data_t;
    signal alu_opcode : alu_opcode_t;
begin

    control: entity work.main_control(behave)
    port map (
        instr => instr_input,
        clk => clk,
        rst => rst,
        src1 => src1,
        src2 => src2,
        mem_op => mem_opcode,
        alu_op => alu_opcode,
        pc_src => pc_src,
        reg_write => reg_write,
        immediate => immediate
    );

    clock: process is
    begin
        clk <= '0';
        loop
            wait for period / 2;
            clk <= not clk;
        end loop;
    end process;

    test: process is
    begin
        rst <= '1';
        wait until rising_edge(clk);
        wait for period / 2;
        rst <= '0';
        instr_input <= "00000000010000110000000110110011";
        wait for period;
        instr_input <= "00010000010000110000000110010011";
        wait for period;
        instr_input <= "00010000010000101000010101100011";
        wait for period;

        wait;
    end process;

end architecture;
