library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.riscv_defs.all;

entity alu_src2_mux is
    port (
        sel : in std_logic;
        imm_data : in src_data_t;
        reg2_data : in src_data_t;
        src2 : out src_data_t
    );
end alu_src2_mux;

architecture behave of alu_src2_mux is
begin
    process (sel, imm_data, reg2_data)
    begin
        case (sel) is
            when SRC2_SEL_REG =>
                src2 <= reg2_data;
            when SRC2_SEL_IMM =>
                src2 <= imm_data;
            when others =>
                src2 <= (others => '0');
        end case;
    end process;
end architecture;
