library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.riscv_defs.all;

-- Arithmetic Logic Unit --
-- 
-- Note: the output data size is the same as the input data size, and any
-- overflow or underflow is ignored, as per the RISC-V spec.
entity alu is
    port (
        opcode : in alu_opcode_t;
        op1 : in src_data_t;
        op2 : in src_data_t;
        result : out src_data_t
    );
end alu;

architecture behave of alu is
    subtype alu_data_tmp is unsigned(REG_WIDTH-1 downto 0);

    signal uresult : alu_data_tmp;

    signal sum : alu_data_tmp;
    signal diff : unsigned(REG_WIDTH downto 0);

    signal data_and : alu_data_tmp;
    signal data_or : alu_data_tmp;
    signal data_xor : alu_data_tmp;

    signal lt : boolean;
    signal ltu : boolean;
    signal eq : boolean;

    signal data_sll : alu_data_tmp;
    signal data_srl : alu_data_tmp;
    signal data_sra : alu_data_tmp;

    signal shamt : natural;
begin
    shamt <= to_integer(unsigned(op2(4 downto 0)));

    sum <= unsigned(op1) + unsigned(op2);
    diff <= unsigned('0' & op1) - unsigned('0' & op2);

    data_and <= unsigned(op1 and op2);
    data_or <= unsigned(op1 or op2);
    data_xor <= unsigned(op1 xor op2);

    ltu <= diff(REG_WIDTH) = '1';
    lt <= (diff(REG_WIDTH) and data_xor(REG_WIDTH-1)) = '1';
    eq <= diff = resize("0000", diff'length);

    data_sll <= shift_left(unsigned(op1), shamt);
    data_srl <= shift_right(unsigned(op1), shamt);
    data_sra <= unsigned(shift_right(signed(op1), shamt));

    operation: process(opcode)
    begin
        case opcode is
            -- AND --
            when ALU_OP_AND =>
                uresult <= data_and;
            -- OR --
            when ALU_OP_OR =>
                uresult <= data_or;
            -- XOR --
            when ALU_OP_XOR =>
                uresult <= data_xor;
            -- Add --
            when ALU_OP_ADD =>
                uresult <= sum;
            -- Subtract --
            when ALU_OP_SUB =>
                uresult <= diff(REG_WIDTH-1 downto 0);
            -- Signed less-than --
            when ALU_OP_SLT =>
                uresult <= (others => '0');
                if (lt) then
                    uresult(0) <= '1';
                end if;
            -- Unsigned less-than --
            when ALU_OP_SLTU =>
                uresult <= (others => '0');
                if (ltu) then
                    uresult(0) <= '1';
                end if;
            -- Signed greater-than or equal --
            when ALU_OP_GE =>
                uresult <= (others => '0');
                if (lt = false and eq) then
                    uresult(0) <= '1';
                end if;
            -- Unsigned greater-than or equal --
            when ALU_OP_GEU =>
                uresult <= (others => '0');
                if (ltu = false and eq) then
                    uresult(0) <= '1';
                end if;
            -- Equality --
            when ALU_OP_EQ =>
                uresult <= (others => '0');
                if (eq) then
                    uresult(0) <= '1';
                end if;
            -- Not equal --
            when ALU_OP_NEQ =>
                uresult <= (others => '0');
                if (eq = false) then
                    uresult(0) <= '1';
                end if;
            -- Logical shift left --
            when ALU_OP_SLL =>
                uresult <= data_sll;
            -- Logical shift right --
            when ALU_OP_SRL =>
                uresult <= data_srl;
            -- Arithmetic shift right --
            when ALU_OP_SRA =>
                uresult <= data_sra;
            -- No operation, forward operand 2 --
            when others =>
                uresult <= unsigned(op2);
        end case;
    end process;

    result <= std_logic_vector(uresult);
end architecture behave;
