library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.riscv_defs.all;

package instr_defs is
    constant INSTR_BASE_WIDTH : natural := 32;
    subtype instruction_t is std_logic_vector(INSTR_BASE_WIDTH-1 downto 0);

    -- opcode is found at the start of every instruction --
    constant OPCODE_I_WIDTH : natural := 7;
    subtype opcode_t is std_logic_vector(OPCODE_I_WIDTH-1 downto 0);

    -- opcode types --
    constant OPCODE_LUI      : opcode_t := "0110111"; -- U-type
    constant OPCODE_AUIPC    : opcode_t := "0010111"; -- U-type
    constant OPCODE_LOAD     : opcode_t := "0000011"; -- I-type
    constant OPCODE_STORE    : opcode_t := "0100011"; -- S-type
    constant OPCODE_OP       : opcode_t := "0110011"; -- R-type
    constant OPCODE_OP_IMM   : opcode_t := "0010011"; -- I-type
    constant OPCODE_BRANCH   : opcode_t := "1100011"; -- B-type
    constant OPCODE_JAL      : opcode_t := "1101111"; -- J-type
    constant OPCODE_JALR     : opcode_t := "1100111"; -- I-type
    constant OPCODE_MISC_MEM : opcode_t := "0001111"; -- I-type
    constant OPCODE_SYSTEM   : opcode_t := "1110011"; -- I-type

    -- Compressed "C" Standard Extension --
    -- compressed opcode --
    constant OPCODE_C_WIDTH : natural := 2;
    subtype opcode_c is std_logic_vector(OPCODE_C_WIDTH-1 downto 0);

    constant OPCODE_C0 : opcode_c := "00";
    constant OPCODE_C1 : opcode_c := "01";
    constant OPCODE_C2 : opcode_c := "10";

    ----------------------------------------------------------------------------
    -- Function fields diffentiate between instructions of the same opcode -----
    ----------------------------------------------------------------------------

    -- Primary 3-bit function field --
    constant FUNCT3_HIGH : natural := 14;
    constant FUNCT3_LOW  : natural := 12;
    subtype funct3_t is std_logic_vector(FUNCT3_HIGH downto FUNCT3_LOW);

    -- OP_LOAD --
    constant FUNCT3_LB      : funct3_t := "000";
    constant FUNCT3_LH      : funct3_t := "001";
    constant FUNCT3_LW      : funct3_t := "010";
    constant FUNCT3_LBU     : funct3_t := "100";
    constant FUNCT3_LHU     : funct3_t := "101";

    -- OP_STORE --
    constant FUNCT3_SB      : funct3_t := "000";
    constant FUNCT3_SH      : funct3_t := "001";
    constant FUNCT3_SW      : funct3_t := "010";

    -- OP_OP --
    constant FUNCT3_ADD     : funct3_t := "000";
    constant FUNCT3_SUB     : funct3_t := "000";
    constant FUNCT3_SLL     : funct3_t := "001";
    constant FUNCT3_SLT     : funct3_t := "010";
    constant FUNCT3_SLTU    : funct3_t := "011";
    constant FUNCT3_XOR     : funct3_t := "100";
    constant FUNCT3_SRL     : funct3_t := "101";
    constant FUNCT3_SRA     : funct3_t := "101";
    constant FUNCT3_OR      : funct3_t := "110";
    constant FUNCT3_AND     : funct3_t := "111";

    -- OP_OP_IMM --
    constant FUNCT3_ADDI    : funct3_t := "000";
    constant FUNCT3_SLTI    : funct3_t := "010";
    constant FUNCT3_SLTIU   : funct3_t := "011";
    constant FUNCT3_XORI    : funct3_t := "100";
    constant FUNCT3_ORI     : funct3_t := "110";
    constant FUNCT3_ANDI    : funct3_t := "111";
    constant FUNCT3_SLLI    : funct3_t := "001";
    constant FUNCT3_SRLI    : funct3_t := "101";
    constant FUNCT3_SRAI    : funct3_t := "101";

    -- OP_BRANCH --
    constant FUNCT3_BEQ     : funct3_t := "000";
    constant FUNCT3_BNE     : funct3_t := "001";
    constant FUNCT3_BLT     : funct3_t := "100";
    constant FUNCT3_BGE     : funct3_t := "101";
    constant FUNCT3_BLTU    : funct3_t := "110";
    constant FUNCT3_BGEU    : funct3_t := "111";

    -- OP_MISC_MEM --
    constant FUNCT3_FENCE   : funct3_t := "000";
    constant FUNCT3_FENCEI  : funct3_t := "001";

    -- OP_SYSTEM --
    constant FUNCT3_ECALL   : funct3_t := "000";
    constant FUNCT3_EBREAK  : funct3_t := "001";
    constant FUNCT3_CSRRW   : funct3_t := "001";
    constant FUNCT3_CSRRS   : funct3_t := "010";
    constant FUNCT3_CSRRC   : funct3_t := "011";
    constant FUNCT3_CSRRWI  : funct3_t := "101";
    constant FUNCT3_CSRRSI  : funct3_t := "110";
    constant FUNCT3_CSRRCI  : funct3_t := "111";

    -- Secondary 7-bit function field --
    constant FUNCT7_HIGH := 31;
    constant FUNCT7_LOW  := 25;
    subtype funct7_t is std_logic_vector(FUNCT7_HIGH downto FUNCT7_LOW);

    -- OP_OP --
    constant FUNCT7_ADD     : funct7_t := "0000000";
    constant FUNCT7_SUB     : funct7_t := "0100000";
    constant FUNCT7_SLL     : funct7_t := "0000000";
    constant FUNCT7_SLT     : funct7_t := "0000000";
    constant FUNCT7_SLTU    : funct7_t := "0000000";
    constant FUNCT7_XOR     : funct7_t := "0000000";
    constant FUNCT7_SRL     : funct7_t := "0000000";
    constant FUNCT7_SRA     : funct7_t := "0100000";
    constant FUNCT7_OR      : funct7_t := "0000000";
    constant FUNCT7_AND     : funct7_t := "0000000";

    -- OP_OP_IMM --
    constant FUNCT7_SLLI    : funct7_t := "0000000";
    constant FUNCT7_SRLI    : funct7_t := "0000000";
    constant FUNCT7_SRAI    : funct7_t := "0100000";

end package;
