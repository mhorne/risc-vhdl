--------------------------------------------------------------------------------
-- main_control.vhd
--
-- The main control signal generator for the processor. It takes an instruction
-- as input and parses it to generate the required control signals for other
-- components in the processor.
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.riscv_defs.all;
use work.instr_defs.all;

entity main_control is
    port (
        -- Input signals --
        instr : in instruction_t;   -- Instruction input
        clk : in std_logic;
        rst : in std_logic;

        -- Output signals --
        src1_sel : out std_logic;   -- ALU first operand select
        src2_sel : out std_logic;   -- ALU second operand select
        alu_op : out alu_opcode_t;  -- ALU operation select
        mem_read : out std_logic;   -- Read from memory
        mem_write : out std_logic;  -- Write to memory
        pc_src : out std_logic;     -- Program counter source select
        reg_write : out std_logic;  -- Register write select
        immediate : out src_data_t  -- Decoded immediate value
    );
end main_control;

architecture behave of main_control is
    -- Internal signals --
    signal imm_type : imm_type_t;
    signal opcode : opcode_t;
    signal funct3 : funct3_t;
    signal funct7 : funct7_t;
begin

    decoder: entity work.imm_decoder(behave)
    port map (
        imm_type => imm_type,
        imm_in => instr(INSTR_BASE_WIDTH-1 downto OPCODE_I_WIDTH),
        imm_out => immediate
    );

    opcode <= instr(OPCODE_I_WIDTH-1 downto 0);
    funct3 <= instr(FUNCT3_HIGH downto FUNCT3_LOW);
    funct7 <= instr(FUNCT7_HIGH downto FUNCT7_LOW);

    update_state: process (clk, rst)
    begin
        if (rst = '1') then
            reg_write <= '0';
            src1_sel <= ALU_SRC1_REG;
            src2_sel <= ALU_SRC2_REG;
            mem_read <= '0';
            mem_write <= '0';
            pc_src <= '0';
        elsif (clk'event and clk = '1') then

            imm_type <= IMM_NONE;
            src1_sel <= ALU_SRC1_REG;
            src2_sel <= ALU_SRC2_REG;
            mem_read <= '0';
            mem_write <= '0';
            reg_write <= '0';
            pc_src <= PC_SRC_SEL_PC;
            alu_op <= ALU_OP_ADD;

            case opcode is
                when OPCODE_LUI =>
                    imm_type <= IMM_U;
                    src2_sel <= ALU_SRC2_IMM;
                    reg_write <= '1';
                when OPCODE_AUIPC =>
                    imm_type <= IMM_U;
                    src1_sel <= ALU_SRC1_PC;
                    src2_sel <= ALU_SRC2_IMM;
                    reg_write <= '1';
                when OPCODE_LOAD =>
                    src2_sel <= ALU_SRC2_IMM;
                    mem_read <= '1';
                    reg_write <= '1';
                when OPCODE_STORE =>
                    imm_type <= IMM_S;
                    src2_sel <= ALU_SRC2_IMM;
                    mem_write <= '1';
                when OPCODE_OP =>
                    reg_write <= '1';
                    case (funct3) is
                        when FUNCT3_ADD =>
                            case (funct7) is
                                when FUNCT7_ADD => alu_op <= ALU_OP_ADD;
                                when FUNCT7_SUB => alu_op <= ALU_OP_SUB;
                                when others => alu_op <= ALU_OP_NONE;
                            end case;
                        when FUNCT3_SLL => alu_op <= ALU_OP_SLL;
                        when FUNCT3_SLT => alu_op <= ALU_OP_SLT;
                        when FUNCT3_SLTU => alu_op <= ALU_OP_SLTU;
                        when FUNCT3_XOR => alu_op <= ALU_OP_XOR;
                        when FUNCT3_SRL =>
                            case (funct7) is
                                when FUNCT7_SRL => alu_op <= ALU_OP_SRL;
                                when FUNCT7_SRA => alu_op <= ALU_OP_SRA;
                                when others => alu_op <= ALU_OP_NONE;
                            end case;
                        when FUNCT3_OR => alu_op <= ALU_OP_OR;
                        when FUNCT3_AND => alu_op <= ALU_OP_AND;
                        when others => alu_op <= ALU_OP_NONE;
                    end case;
                when OPCODE_OP_IMM =>
                    imm_type <= IMM_I;
                    src2_sel <= ALU_SRC2_IMM;
                    reg_write <= '1';
                    case (funct3) is
                        when FUNCT3_ADDI => alu_op <= ALU_OP_ADD;
                        when FUNCT3_SLTI => alu_op <= ALU_OP_SLT;
                        when FUNCT3_SLTIU => alu_op <= ALU_OP_SLTU;
                        when FUNCT3_XORI => alu_op <= ALU_OP_XOR;
                        when FUNCT3_ORI => alu_op <= ALU_OP_OR;
                        when FUNCT3_ANDI => alu_op <= ALU_OP_AND;
                        when FUNCT3_SLLI => alu_op <= ALU_OP_SLL;
                        when FUNCT3_SRL =>
                            case (funct7) is
                                when FUNCT7_SRL => alu_op <= ALU_OP_SRL;
                                when FUNCT7_SRA => alu_op <= ALU_OP_SRA;
                                when others => alu_op <= ALU_OP_NONE;
                            end case;
                        when others => alu_op <= ALU_OP_NONE;
                    end case;
                when OPCODE_BRANCH =>
                    imm_type <= IMM_B;
                    src2_sel <= ALU_SRC2_IMM;
                    pc_src <= PC_SRC_SEL_ALU;
                    case (funct3) is
                        when FUNCT3_BEQ => alu_op <= ALU_OP_EQ;
                        when FUNCT3_BNE => alu_op <= ALU_OP_NEQ;
                        when FUNCT3_BLT => alu_op <= ALU_OP_SLT;
                        when FUNCT3_BGE => alu_op <= ALU_OP_GE;
                        when FUNCT3_BLTU => alu_op <= ALU_OP_SLTU;
                        when FUNCT3_BGEU => alu_op <= ALU_OP_GEU;
                        when others => alu_op <= ALU_OP_NONE;
                    end case;
                when OPCODE_JAL =>
                    imm_type <= IMM_J;
                    src1_sel <= ALU_SRC1_PC;
                    src2_sel <= ALU_SRC2_IMM;
                    pc_src <= PC_SRC_SEL_ALU;
                when OPCODE_JALR =>
                    imm_type <= IMM_I;
                    src1_sel <= ALU_SRC1_PC;
                    src2_sel <= ALU_SRC2_IMM;
                    reg_write <= '1';
                    pc_src <= PC_SRC_SEL_ALU;
                    alu_op <= ALU_OP_ADD;
                when others =>
            end case;
        end if;
    end process;
end architecture;
