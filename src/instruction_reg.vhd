library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.riscv_defs.all;
use work.instr_defs.all;

-- The Instruction Register --

-- Stores the value of the current instruction to be decoded in the
-- next stage. Some of the outputs overlap as the bits are interpreted
-- differently depending on the instruction format.
entity instruction_reg is
    port (
        clk : in std_logic; -- Clock
        rst : in std_logic; -- Reset
        en : in std_logic;  -- Enable
        data_in : in instruction_t; -- Input instruction

        r1_out : out reg_addr_t; -- First register address
        r2_out : out reg_addr_t; -- Second register address
        rd_out : out reg_addr_t; -- Destination register address

        data_out : out instruction_t -- Output instruction
    );
end instruction_reg;

architecture behave of instruction_reg is
    signal data_tmp : instruction_t;
begin
    write: process (clk, rst)
    begin
        if (rst = '1') then
            data_tmp <= (others => '0');
        elsif (clk'event and clk = '1') then
            if (en = '1') then
                data_tmp <= data_in;
            end if;
        end if;
    end process;

    r1_out <= data_tmp(19 downto 15);
    r2_out <= data_tmp(24 downto 20);
    rd_out <= data_tmp(11 downto 7);

    data_out <= data_tmp;
end architecture;
