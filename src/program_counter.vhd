library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.riscv_defs.all;

entity program_counter is
    port (
        clk : in std_logic;
        rst : in std_logic;
        en : in std_logic;
        data_in : in mem_addr_t;
        data_out : out mem_addr_t;
    );
end program_counter;

architecture behave of program_counter is
    signal data_tmp : reg_data;
begin
    write: process (clk, rst)
    begin
        if (rst = '1') then
            data_tmp <= (others => '0');
        elsif (clk'event and clk = '1') then
            if (en = '1') then
                data_tmp <= data_in;
            end if;
        end if;
    end process;

    data_out <= data_tmp;
end architecture;
