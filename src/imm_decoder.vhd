library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.riscv_defs.all;
use work.instr_defs.all;

entity imm_decoder is
    port (
        imm_type : in imm_type_t;
        imm_in : in std_logic_vector(INSTR_BASE_WIDTH-1 downto OPCODE_I_WIDTH);
        imm_out : out src_data_t
    );
end imm_decoder;

architecture behave of imm_decoder is
    signal full_data : signed(REG_WIDTH-1 downto 0);
begin
    decode: process (imm_type, imm_in)
        variable tmp_data : signed(31 downto 0);
    begin
        -- Assign immediate bits based on type --
        case imm_type is
            when IMM_I =>
                tmp_data(11 downto 0) := signed(imm_in(31 downto 20));
                tmp_data(31 downto 12) := (others => '0');
                full_data <= resize(tmp_data, full_data'length);
            when IMM_S =>
                tmp_data(4 downto 0) := signed(imm_in(11 downto 7));
                tmp_data(11 downto 5) := signed(imm_in(31 downto 25));
                tmp_data(31 downto 12) := (others => '0');
                full_data <= resize(tmp_data, full_data'length);
            when IMM_B =>
                tmp_data(0) := '0';
                tmp_data(4 downto 1) := signed(imm_in(11 downto 8));
                tmp_data(10 downto 5) := signed(imm_in(30 downto 25));
                tmp_data(11) := imm_in(7);
                tmp_data(12) := imm_in(31);
                tmp_data(31 downto 13) := (others => '0');
                full_data <= resize(tmp_data, full_data'length);
            when IMM_U =>
                tmp_data(11 downto 0) := (others => '0');
                tmp_data(31 downto 12) := signed(imm_in(31 downto 12));
                full_data <= resize(tmp_data, full_data'length);
            when IMM_J =>
                tmp_data(0) := '0';
                tmp_data(10 downto 1) := signed(imm_in(30 downto 21));
                tmp_data(11) := imm_in(20);
                tmp_data(19 downto 12) := signed(imm_in(19 downto 12));
                tmp_data(20) := imm_in(31);
                tmp_data(31 downto 21) := (others => '0');
                full_data <= resize(tmp_data, full_data'length);
            when IMM_NONE =>
                tmp_data := (others => '0');
                full_data <= resize(tmp_data, full_data'length);
            when others =>
                tmp_data := (others => '0');
                full_data <= resize(tmp_data, full_data'length);
        end case;
    end process;

    imm_out <= std_logic_vector(full_data);
end behave;
