library ieee;
use ieee.std_logic_1164.all;

package riscv_defs is
    -----------------------------------
    -- Constants ----------------------
    -----------------------------------
    constant BASE_WIDTH : natural := 32;
    constant WORD_SIZE : natural := 4;

    constant REG_WIDTH : natural := BASE_WIDTH; -- General purpose register width
    constant REG_ADDR_WIDTH : natural := 5;     -- 2^REG_ADDR_WIDTH = REGFILE_SIZE
    constant REGFILE_SIZE : natural := 32;      -- Number of GPRs

    constant MEM_ADDR_WIDTH : natural := BASE_WIDTH;

    -----------------------------------
    -- Subtypes -----------------------
    -----------------------------------
    -- data to/from register or ALU
    subtype src_data_t is std_logic_vector(REG_WIDTH-1 downto 0);
    -- register address width
    subtype reg_addr_t is std_logic_vector(REG_ADDR_WIDTH-1 downto 0);

    subtype mem_addr_t is std_logic_vector(MEM_ADDR_WIDTH-1 downto 0);

    -----------------------------------
    -- Enumerations -------------------
    -----------------------------------
    constant ALU_OP_WIDTH : natural := 4;
    subtype alu_opcode_t : std_logic_vector(ALU_OP_WIDTH-1 downto 0);

    constant ALU_OP_NONE : alu_opcode_t := "0000"; -- forwards operand 2
    constant ALU_OP_AND  : alu_opcode_t := "0001"; -- logical AND
    constant ALU_OP_OR   : alu_opcode_t := "0010"; -- logical OR
    constant ALU_OP_XOR  : alu_opcode_t := "0011"; -- logical XOR
    constant ALU_OP_ADD  : alu_opcode_t := "0100"; -- addition
    constant ALU_OP_SUB  : alu_opcode_t := "0101"; -- subtraction
    constant ALU_OP_SLT  : alu_opcode_t := "0110"; -- signed less-than
    constant ALU_OP_SLTU : alu_opcode_t := "0111"; -- unsigned less-than
    constant ALU_OP_GE   : alu_opcode_t := "1000"; -- signed greater-than or equal
    constant ALU_OP_GEU  : alu_opcode_t := "1001"; -- unsigned greater-than or equal
    constant ALU_OP_SLL  : alu_opcode_t := "1010"; -- logical shift left
    constant ALU_OP_SRL  : alu_opcode_t := "1011"; -- logical shift right
    constant ALU_OP_SRA  : alu_opcode_t := "1100"; -- arithmetic shift right
    constant ALU_OP_EQ   : alu_opcode_t := "1101"; -- equal
    constant ALU_OP_NEQ  : alu_opcode_t := "1110"; -- not equal

    -- ALU source for operand 1 --
    constant SRC1_SEL_REG : std_logic := '0'; -- output 1 from regfile
    constant SRC1_SEL_PC  : std_logic := '1'; -- program counter

    -- ALU source for operand 2 --
    constant SRC2_SEL_REG : std_logic := '0'; -- output 2 from regfile
    constant SRC2_SEL_IMM : std_logic := '1'; -- immediate

    -- PC source --
    constant PC_SRC_SEL_PC  : std_logic := '0'; -- program counter + 4
    constant PC_SRC_SEL_ALU : std_logic := '1'; -- output from ALU

    -- Types of immediate
    constant IMM_TYPE_WIDTH : natural := 3;
    subtype imm_type_t : std_logic_vector(IMM_TYPE_WIDTH-1 downto 0);

    constant IMM_NONE : imm_type_t := "000"; -- No immediate
    constant IMM_I    : imm_type_t := "001"; -- I-type immediate
    constant IMM_S    : imm_type_t := "010"; -- S-type immediate
    constant IMM_B    : imm_type_t := "011"; -- B-type immediate
    constant IMM_U    : imm_type_t := "100"; -- U-type immediate
    constant IMM_J    : imm_type_t := "101"; -- J-type immediate

end package;
