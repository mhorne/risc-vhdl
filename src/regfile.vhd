library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.riscv_defs.all;

entity regfile is
    port (
        clk : in std_logic;
        rst : in std_logic;

        r_addr1 : in reg_addr_t; -- read port select 1
        r_addr2 : in reg_addr_t; -- read port select 2

        w_addr : in reg_addr_t; -- write port select
        w_data : in src_data_t; -- input data
        write : in std_logic;   -- write control

        r_data1 : out src_data_t; -- output data 1
        r_data2 : out src_data_t  -- output data 2
    );
end regfile;

architecture behave of regfile is
    type registers is array (REGFILE_SIZE-1 downto 0) of src_data_t;
    signal regdata : registers;
begin

    -- reg0 is specified to only store zero --
    regdata(0) <= (others => '0');

    write_to_reg: process (clk, rst)
    begin
        if (rst = '1') then
            for i in 1 to REGFILE_SIZE-1 loop
                regdata(i) <= (others => '0');
            end loop;
        elsif (clk'event and clk = '1') then
            if (write = '1' and unsigned(w_addr) > 0) then
                regdata(to_integer(unsigned(w_addr))) <= w_data;
            end if;

            r_data1 <= regdata(to_integer(unsigned(r_addr1)));
            r_data2 <= regdata(to_integer(unsigned(r_addr2)));
        end if;
    end process;

end behave;
