library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.riscv_defs.all;

entity buffer_reg is
    generic (
        N : integer := REG_WIDTH);
    port (
        clk : in std_logic; -- Clock
        rst : in std_logic; -- Reset
        en : in std_logic;  -- Enable
        data_in : in std_logic_vector(N-1 downto 0);  -- Input data
        data_out : out std_logic_vector(N-1 downto 0) -- Output data
    );
end buffer_reg;

architecture behave of buffer_reg is
    signal data_tmp : std_logic_vector(N-1 downto 0);
begin
    write: process (clk, rst)
    begin
        if (rst = '1') then
            data_tmp <= (others => '0');
        elsif (clk'event and clk = '1') then
            if (en = '1') then
                data_tmp <= data_in;
            end if;
        end if;
    end process;

    data_out <= data_tmp;
end architecture;
