library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity shift_reg is
    generic (
        DATA_WIDTH : integer;
        LEVELS : integer
    );
    port (
        clk : in std_logic;
        rst : in std_logic;
        en : in std_logic;
        data_in : in std_logic_vector(DATA_WIDTH-1 downto 0);
        data_out : out std_logic_vector(DATA_WIDTH-1 downto 0)
    );
end shift_reg;

architecture behave of shift_reg is
    type sreg_array is array (0 to LEVELS-1) of std_logic_vector(DATA_WIDTH-1 downto 0);
    signal sreg_data : sreg_array;
begin
    process (clk, rst)
    begin
        if (rst = '1') then
            sreg_data <= (others => (others => '0'));
        elsif (clk'event and clk = '1') then
            if (en = '1') then
                for i in 1 to LEVELS-1 loop
                    sreg_data(i) <= sreg_data(i-1);
                end loop;
                sreg_data(0) <= data_in;
                data_out <= sreg_data(LEVELS-1);
            end if;
        end if;
    end process;
end architecture;
