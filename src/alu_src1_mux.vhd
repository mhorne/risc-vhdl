library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.riscv_defs.all;

entity alu_src1_mux is
    port (
        sel : in std_logic;
        pc_data : in src_data_t;
        reg1_data : in src_data_t;
        src1 : out src_data_t
    );
end alu_src1_mux;

architecture behave of alu_src1_mux is
begin
    process (sel, pc_data, reg1_data)
    begin
        case (sel) is
            when SRC1_SEL_REG =>
                src1 <= reg1_data;
            when SRC1_SEL_PC =>
                src1 <= pc_data;
            when others =>
                src1 <= (others => '0');
        end case;
    end process;
end architecture;
