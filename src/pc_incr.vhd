--------------------------------------------------------------------------------
-- pc_incr.vhd
--
-- The program counter incrementer takes the current value of the program
-- counter and increases it by one word to address the next instruction. It
-- registers the output, and is intended to operate during the instruction-fetch
-- stage.
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.riscv_defs.all;

entity pc_incr is
    port (
        clk : in std_logic;
        rst : in std_logic;
        en  : in std_logic;
        addr_in : in mem_addr_t;
        addr_out : out mem_addr_t
    );
end pc_incr;

architecture behave of pc_incr is
    signal addr_tmp : unsigned(MEM_ADDR_WIDTH-1 downto 0);
begin

    process (clk, rst)
    begin
        if (rst = '1') then
            addr_out <= (others => '0');
        elsif (clk = '1' and clk'event) then
            if (en = '1') then
                addr_tmp <= unsigned(addr_in) + 4;
            end if;
        end if;
    end process;

    addr_out <= std_logic_vector(addr_tmp);
end architecture;
